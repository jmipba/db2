#!/bin/bash
set -e
#
#   Initialize DB2 instance in a Docker container
#
# # Authors:
#   * Leo (Zhong Yu) Wu       <leow@ca.ibm.com>
#
# Copyright 2015, IBM Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -z "$DB2INST1_PASSWORD" ]; then
  echo ""
  echo >&2 'error: DB2INST1_PASSWORD not set'
  echo >&2 'Did you forget to add -e DB2INST1_PASSWORD=... ?'
  exit 1
else
  echo -e "$DB2INST1_PASSWORD\n$DB2INST1_PASSWORD" | passwd db2inst1
fi

if [ -z "$LICENSE" ];then
   echo ""
   echo >&2 'error: LICENSE not set'
   echo >&2 "Did you forget to add '-e LICENSE=accept' ?"
   exit 1
fi

if [ "${LICENSE}" != "accept" ];then
   echo ""
   echo >&2 "error: LICENSE not set to 'accept'"
   echo >&2 "Please set '-e LICENSE=accept' to accept License before use the DB2 software contained in this image."
   exit 1
fi

if [[ $1 = "db2start" ]]; then
  su - db2inst1 -c "db2start"
  nohup /usr/sbin/sshd -D 2>&1 > /dev/null &
  while true; do sleep 1000; done
elif [[ $1 = "db2sampl" ]]; then
  echo "Starting DB instance..."
  su - db2inst1 -c "db2start"
  if [ ! -f "/home/created_bd" ]; then
	  echo "Creating sample DB..."
	  su - db2inst1 -c "db2 create db sample"
      touch /home/created_bd
	  echo "Connecting to DB & Creating table..."
	  echo "Executing callejero.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/callejero/callejero.sql"
	  echo "Executing municipios.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/callejero/municipios.sql"
	  echo "Executing paises.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/callejero/paises.sql"
	  echo "Executing calculoIntereses.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/calculoIntereses.sql"
	  echo "Executing contraido.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/contraido.sql"
	  echo "Executing contraidoDatoVariable.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/contraidoDatoVariable.sql"
	  echo "Executing ejecucion0.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/ejecucion0.sql"
	  echo "Executing ejecuciones0Relacionadas.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/ejecuciones0Relacionadas.sql"
	  echo "Executing ejecucionImpar.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/ejecucionImpar.sql"
	  echo "Executing ejecucionPar.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/ejecucionPar.sql"
	  echo "Executing estadoEjecucion0.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/estadoEjecucion0.sql"
	  echo "Executing incidencia.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/incidencia.sql"
	  echo "Executing intereses.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/intereses.sql"
	  echo "Executing interrupcionPrescripcion.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/interrupcionPrescripcion.sql"
	  echo "Executing obligadoTributario.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/obligadoTributario.sql"
	  echo "Executing operacionRealizadaRecaudacion.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/operacionRealizadaRecaudacion.sql"
	  echo "Executing partida0.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/partida0.sql"
	  echo "Executing partidaImpar.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/partidaImpar.sql"
	  echo "Executing partidaPar.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/partidaPar.sql"
	  echo "Executing recargo.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/recargo.sql"
	  echo "Executing tramosIntereses.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/contraido/tramosIntereses.sql"
	  echo "Executing i18n.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/i18n/i18n.sql"
	  echo "Executing codigoAplicacion.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/codigoAplicacion.sql"
	  echo "Executing lote.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/lote.sql"
	  echo "Executing loteAltaCont.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/loteAltaCont.sql"
	  echo "Executing registroLote.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/registroLote.sql"
	  echo "Executing servicio.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/servicio.sql"
	  #echo "Executing HKOTT01_OPER_REAL.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT01_OPER_REAL.sql"
	  #echo "Executing HKOTT02_OPREA_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT02_OPREA_ELEM.sql"
	  #echo "Executing HKOTT03_INTERVIN.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT03_INTERVIN.sql"
	  #echo "Executing HKOTT04_INTER_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT04_INTER_ELEM.sql"
	  #echo "Executing HKOTT05_FIRM_AUTO.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT05_FIRM_AUTO.sql"
	  #echo "Executing HKOTT06_FIRAU_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT06_FIRAU_ELEM.sql"
	  #echo "Executing HKOTT07_DOCUMENTOS.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT07_DOCUMENTOS.sql"
	  #echo "Executing HKOTT08_DOCUM_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT08_DOCUM_ELEM.sql"
	  #echo "Executing HKOTT09_DOCU_ENTR.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT09_DOCU_ENTR.sql"
	  #echo "Executing HKOTT10_DOCU_SALI.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT10_DOCU_SALI.sql"
	  #echo "Executing HKOTT11_DOCUM_ADJ.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT11_DOCUM_ADJ.sql"
	  #echo "Executing HKOTT12_DOCAD_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT12_DOCAD_ELEM.sql"
	  #echo "Executing HKOTT13_ACTO_ADMIN.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT13_ACTO_ADMIN.sql"
	  #echo "Executing HKOTT14_ACTAD_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT14_ACTAD_ELEM.sql"
	  #echo "Executing HKOTT15_SOLICITUD.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT15_SOLICITUD.sql"
	  #echo "Executing HKOTT16_SOLIC_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT16_SOLIC_ELEM.sql"
	  #echo "Executing HKOTT17_FINALIZAC.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT17_FINALIZAC.sql"
	  #echo "Executing HKOTT18_FINAL_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT18_FINAL_ELEM.sql"
	  #echo "Executing HKOTT19_PLAZOS.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT19_PLAZOS.sql"
	  #echo "Executing HKOTT20_PLAZO_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT20_PLAZO_ELEM.sql"
	  #echo "Executing HKOTT21_MOTIVACION.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT21_MOTIVACION.sql"
	  #echo "Executing HKOTT22_MOTIV_ELEM.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/PS3_Elementos_Transversales/HKOTT22_MOTIV_ELEM.sql"
	  echo "Executing administracion.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/administracion.sql"
	  echo "Executing gestionelementos.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/gestionelementos.sql"
	  #echo "Executing secuencias.sql"
	  #su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/modelador/secuencias.sql"
	  echo "Executing direccion.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/persona/direccion.sql"
	  echo "Executing email.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/persona/email.sql"
	  echo "Executing calletelefonojero.sql"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/persona/telefono.sql"
	
	  echo "Inserting data into HKOAS0001, HKOAS0101, HKOAS0201, HKOAS0301, HKOAS0501"
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/administracion.sql"
	
	  su - db2inst1 -c "db2 connect to sample && db2 -tvf /home/db2inst1/config/lote/secuencias.sql"
	
	  echo "Initialize complete"
  fi
 
  nohup /usr/sbin/sshd -D 2>&1 > /dev/null &
  while true; do sleep 1000; done
else
  exec "$1"
fi
