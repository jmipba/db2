CREATE TABLE HKOTS08_TRADI18N  
    (
      KOS08UID		DECIMAL (15,0)			NOT NULL  
     ,KOS08CLAVE		VARCHAR  (120)		NOT NULL  
     ,KOS08IDIOMA		VARCHAR  (5)				NOT NULL  
     ,KOS08TRAD 		VARCHAR  (200)		NOT NULL  
    );