CREATE TABLE HKOAN0301 
    (
     KON03UID			DECIMAL (15,0)	NOT NULL  
     ,KON03ELEMDESTIN	DECIMAL (20,0)	NOT NULL  
     ,KON03S00TIPELEM	DECIMAL (15,0)	NOT NULL  
     ,KON03TELEFONO		CHAR (13)		NOT NULL  
     ,KON03PRIORIT		CHAR (1)		NOT NULL  
     ,KON03FALTA		TIMESTAMP		NOT NULL  
     ,KON03FBAJA		TIMESTAMP		NOT NULL
     ,KON03ACTIVO		CHAR (1)		NOT NULL
     ,KON03CDATE		TIMESTAMP		NOT NULL  
     ,KON03CUSER		CHAR (10)		NOT NULL  
     ,KON03MDATE		TIMESTAMP		NOT NULL  
     ,KON03MUSER		CHAR (10)		NOT NULL  
     ,KON03DELETED		CHAR (1)		NOT NULL
    )
;