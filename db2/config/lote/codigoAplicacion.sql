-- HSIT300_COD_APLIC

CREATE TABLE HSIA30002
    (
     T300CAPL			CHAR (2)	  
     ,T300TIPO   		CHAR (1)	  
     ,T300NOMAPL 		CHAR (30)	  
     ,T300NOMAPE 		CHAR (30)	  
     ,T300CONC   		CHAR (7)	  
     ,T300OFIDES 		CHAR (32)		  
     ,T300CARGO  		CHAR (1)	
     ,T300EMISION		CHAR (1)	  
     ,T300TIPODEV		CHAR (1)	  
     ,T300APUN   		CHAR (20)		  
     ,T300CODOFI 		CHAR (2)	
     ,T300DATVAR 		CHAR (1)		
     ,T300PROCED 		CHAR (1)	  
     ,T300PROBAT 		CHAR (1)		  
     ,T300SINBOR 		CHAR (1)		  
     ,T300MODNOT 		CHAR (1)		  
     ,T300TRANSF 		CHAR (1)		
     ,T300NOTEJE 		CHAR (1)		
     ,T300ORIGEN 		CHAR (5)		
     ,T300FECALT 		CHAR (8)		
     ,T300FECBAJ 		CHAR (8)		
     ,T300CODUG  		CHAR (2)		
    ) 
;

