CREATE TABLE HKOAL0201 (
     KOL02UID			DECIMAL (15,0)	NOT NULL  
     ,KOL02L00LOTE		DECIMAL (20,0)	NOT NULL  
     ,KOL02CODAPL		VARCHAR (4)		NOT NULL  
     ,KOL02CODSER		VARCHAR (4)		NOT NULL  
     ,KOL02REQAPR		VARCHAR (1)		NOT NULL
     ,KOL02REQFIR		VARCHAR (1)		NOT NULL
     ,KOL02SUJALE		VARCHAR (1)		NOT NULL
     ,KOL02NOTVOL		VARCHAR (1)		NOT NULL
     ,KOL02NACEJE		VARCHAR (1)		NOT NULL
     ,KOL02NOPRAP		VARCHAR (1)		NOT NULL
     ,KOL02APRUEBA		VARCHAR (4)		NOT NULL  
     ,KOL02FIRMA		VARCHAR (4)		NOT NULL  
     ,KOL02CDATE		TIMESTAMP		NOT NULL  
     ,KOL02CUSER		CHAR (10)		NOT NULL  
     ,KOL02MDATE		TIMESTAMP		NOT NULL  
     ,KOL02MUSER		CHAR (10)		NOT NULL  
     ,KOL02DELETED		CHAR (1)		NOT NULL 
    ) 
;

CREATE SEQUENCE HKOQL02_UID 
    AS BIGINT 
    START WITH 1 
	INCREMENT BY  1;