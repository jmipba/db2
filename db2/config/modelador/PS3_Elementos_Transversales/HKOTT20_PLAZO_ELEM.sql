
CREATE TABLE HKOTT20_PLAZO_ELEM 
    (PRIMARY KEY(KOT20UID)
--	 ,FOREIGN KEY REHKOT201(KOT20T19PLAZO)
--		REFERENCES HKOTT19_PLAZOS
     ,KOT20UID			DECIMAL (15,0)		NOT NULL  
     ,KOT20T19PLAZO		DECIMAL (20,0)	NOT NULL  
     ,KOT20ELEMREL		DECIMAL (20,0)	NOT NULL  
     ,KOT20S00TIPELEM	DECIMAL (15,0)	NOT NULL  
     ,KOT20S01MOTIREL	DECIMAL (15,0)	NOT NULL 
     ,KOT20FINICIO		TIMESTAMP		NOT NULL  
     ,KOT20FFIN			TIMESTAMP		NOT NULL WITH DEFAULT 
										'1900-01-01-00.00.00.000000'  
     ,KOT20ACTIVO		CHAR (1)		NOT NULL WITH DEFAULT 'S'  
     ,KOT20S01DOMINIO	DECIMAL (15,0)	NOT NULL  
     ,KOT20CDATE		TIMESTAMP		NOT NULL  
     ,KOT20CUSER		CHAR (10)		NOT NULL  
     ,KOT20MDATE		TIMESTAMP		NOT NULL  
     ,KOT20MUSER		CHAR (10)		NOT NULL  
     ,KOT20DELETED		CHAR (1)		NOT NULL WITH DEFAULT 'N' 
    ) 

;

/*COMMENT ON TABLE HKOTT20_PLAZO_ELEM IS 'PLAZOS-ELEMENTOS'
;
*/

/*COMMENT ON HKOTT20_PLAZO_ELEM (
	 KOT20UID			IS 'Id. único'
	,KOT20T19PLAZO		IS 'Elemento motivación'
	,KOT20ELEMREL		IS 'Id. del elemento relacionado'
	,KOT20S00TIPELEM	IS 'Tipo de elemento'
	,KOT20S01MOTIREL	IS 'Motivo relación'
	,KOT20FINICIO		IS 'Fecha inicio relación'
	,KOT20FFIN			IS 'Fecha fin de la relación'
	,KOT20ACTIVO		IS 'Booleano elemento activo'
	,KOT20S01DOMINIO	IS 'Dominio pertenece elemento'
	,KOT20CDATE			IS 'Fecha creación registro'
	,KOT20CUSER			IS 'Usuario creación registro'
	,KOT20MDATE			IS 'Fecha última modif. registro'
	,KOT20MUSER			IS 'Usuario última modif. registro'
	,KOT20DELETED		IS 'Borrado lógico registro'
	);*/


CREATE VIEW HKOWT2001 AS
SELECT
     KOT20UID
   , KOT20T19PLAZO
   , KOT20ELEMREL
   , KOT20S00TIPELEM
   , KOT20S01MOTIREL
   , KOT20FINICIO
   , KOT20FFIN
   , KOT20ACTIVO
   , KOT20S01DOMINIO
   , KOT20CDATE
   , KOT20CUSER
   , KOT20MDATE
   , KOT20MUSER
   , KOT20DELETED
 FROM 
    HKOTT20_PLAZO_ELEM 


WHERE KOT20DELETED = 'N';

CREATE SYNONYM HKOAT2001 FOR HKOWT2001;


CREATE VIEW HKOWT20DE AS
SELECT
     KOT20UID
   , KOT20T19PLAZO
   , KOT20ELEMREL
   , KOT20S00TIPELEM
   , KOT20S01MOTIREL
   , KOT20FINICIO
   , KOT20FFIN
   , KOT20ACTIVO
   , KOT20S01DOMINIO
   , KOT20CDATE
   , KOT20CUSER
   , KOT20MDATE
   , KOT20MUSER
   , KOT20DELETED
 FROM 
    HKOTT20_PLAZO_ELEM ;

CREATE SYNONYM HKOAT20DE FOR HKOWT20DE;

CREATE UNIQUE INDEX HKOIT201UID 
    ON HKOTT20_PLAZO_ELEM 
    ( KOT20UID )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT202PLA 
    ON HKOTT20_PLAZO_ELEM 
    ( KOT20T19PLAZO )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT203ELR 
    ON HKOTT20_PLAZO_ELEM 
    ( KOT20ELEMREL )
	
--		  CLOSE NO
--    CLUSTER 
;

CREATE INDEX HKOIT204MRE 
    ON HKOTT20_PLAZO_ELEM 
    ( KOT20S01MOTIREL )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT205TEL 
    ON HKOTT20_PLAZO_ELEM 
    ( KOT20S00TIPELEM )
	
--		  CLOSE NO
;

