
CREATE TABLE HKOTT09_DOCU_ENTR 
    (PRIMARY KEY(KOT09T07ELEMUID)
--	 ,FOREIGN KEY REHKOT091(KOT09T07ELEMUID)
--		REFERENCES HKOTT07_DOCUMENTOS
     ,KOT09T07ELEMUID	DECIMAL (20,0)	NOT NULL  
     ,KOT09S01DOMINIO	DECIMAL (15,0)	NOT NULL  
     ,KOT09CDATE		TIMESTAMP		NOT NULL  
     ,KOT09CUSER		CHAR (10)		NOT NULL  
     ,KOT09MDATE		TIMESTAMP		NOT NULL  
     ,KOT09MUSER		CHAR (10)		NOT NULL  
     ,KOT09DELETED		CHAR (1)		NOT NULL WITH DEFAULT 'N' 
    ) 

;

/*COMMENT ON TABLE HKOTT09_DOCU_ENTR IS 'DOCUMENTOS ENTRADA'
;
*/

/*COMMENT ON HKOTT09_DOCU_ENTR (
	 KOT09T07ELEMUID	IS 'Id. único del elemento'
	,KOT09S01DOMINIO	IS 'Dominio pertenece elemento'
	,KOT09CDATE			IS 'Fecha creación registro'
	,KOT09CUSER			IS 'Usuario creación registro'
	,KOT09MDATE			IS 'Fecha última modif. registro'
	,KOT09MUSER			IS 'Usuario última modif. registro'
	,KOT09DELETED		IS 'Borrado lógico registro'
	);*/


CREATE VIEW HKOWT0901 AS
SELECT
     KOT09T07ELEMUID
   , KOT09S01DOMINIO
   , KOT09CDATE
   , KOT09CUSER
   , KOT09MDATE
   , KOT09MUSER
   , KOT09DELETED
 FROM 
    HKOTT09_DOCU_ENTR 


WHERE KOT09DELETED = 'N';

CREATE SYNONYM HKOAT0901 FOR HKOWT0901;


CREATE VIEW HKOWT09DE AS
SELECT
     KOT09T07ELEMUID
   , KOT09S01DOMINIO
   , KOT09CDATE
   , KOT09CUSER
   , KOT09MDATE
   , KOT09MUSER
   , KOT09DELETED
 FROM 
    HKOTT09_DOCU_ENTR ;

CREATE SYNONYM HKOAT09DE FOR HKOWT09DE;

CREATE UNIQUE INDEX HKOIT091UID 
    ON HKOTT09_DOCU_ENTR 
    ( KOT09T07ELEMUID )
	
--		  CLOSE NO
--    CLUSTER 
;

