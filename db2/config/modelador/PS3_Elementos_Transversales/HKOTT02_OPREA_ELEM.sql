CREATE TABLE HKOTT02_OPREA_ELEM 
    (PRIMARY KEY(KOT02UID)
     ,KOT02UID			DECIMAL (15,0)		NOT NULL  
     ,KOT02T01OPREAL	DECIMAL (20,0)	NOT NULL  
     ,KOT02ELEMREL		DECIMAL (20,0)	NOT NULL  
     ,KOT02S00TIPELEM	DECIMAL (15,0)	NOT NULL  
     ,KOT02S01MOTIREL	DECIMAL (15,0)	NOT NULL 
     ,KOT02FINICIO		TIMESTAMP		NOT NULL  
     ,KOT02FFIN			TIMESTAMP		NOT NULL WITH DEFAULT 
										'1900-01-01-00.00.00.000000'  
     ,KOT02ACTIVO		CHAR (1)		NOT NULL WITH DEFAULT 'S'  
     ,KOT02S01DOMINIO	DECIMAL (15,0)	NOT NULL  
     ,KOT02CDATE		TIMESTAMP		NOT NULL  
     ,KOT02CUSER		CHAR (10)		NOT NULL  
     ,KOT02MDATE		TIMESTAMP		NOT NULL  
     ,KOT02MUSER		CHAR (10)		NOT NULL  
     ,KOT02DELETED		CHAR (1)		NOT NULL WITH DEFAULT 'N' 
    ) 

;

/*COMMENT ON TABLE HKOTT02_OPREA_ELEM IS 'OPER. REALIZADAS-ELEMENTOS'
;
*/

/*COMMENT ON HKOTT02_OPREA_ELEM (
	 KOT02UID			IS 'Id. único'
	,KOT02T01OPREAL		IS 'Elemento operación realizada'
	,KOT02ELEMREL		IS 'Id. del elemento relacionado'
	,KOT02S00TIPELEM	IS 'Tipo de elemento'
	,KOT02S01MOTIREL	IS 'Motivo relación'
	,KOT02FINICIO		IS 'Fecha inicio relación'
	,KOT02FFIN			IS 'Fecha fin de la relación'
	,KOT02ACTIVO		IS 'Booleano elemento activo'
	,KOT02S01DOMINIO	IS 'Dominio pertenece elemento'
	,KOT02CDATE			IS 'Fecha creación registro'
	,KOT02CUSER			IS 'Usuario creación registro'
	,KOT02MDATE			IS 'Fecha última modif. registro'
	,KOT02MUSER			IS 'Usuario última modif. registro'
	,KOT02DELETED		IS 'Borrado lógico registro'
	);*/


CREATE VIEW HKOWT0201 AS
SELECT
     KOT02UID
   , KOT02T01OPREAL
   , KOT02ELEMREL
   , KOT02S00TIPELEM
   , KOT02S01MOTIREL
   , KOT02FINICIO
   , KOT02FFIN
   , KOT02ACTIVO
   , KOT02S01DOMINIO
   , KOT02CDATE
   , KOT02CUSER
   , KOT02MDATE
   , KOT02MUSER
   , KOT02DELETED
 FROM 
    HKOTT02_OPREA_ELEM 


WHERE KOT02DELETED = 'N';

CREATE SYNONYM HKOAT0201 FOR HKOWT0201;


CREATE VIEW HKOWT02DE AS
SELECT
     KOT02UID
   , KOT02T01OPREAL
   , KOT02ELEMREL
   , KOT02S00TIPELEM
   , KOT02S01MOTIREL
   , KOT02FINICIO
   , KOT02FFIN
   , KOT02ACTIVO
   , KOT02S01DOMINIO
   , KOT02CDATE
   , KOT02CUSER
   , KOT02MDATE
   , KOT02MUSER
   , KOT02DELETED
 FROM 
    HKOTT02_OPREA_ELEM ;

CREATE SYNONYM HKOAT02DE FOR HKOWT02DE;

CREATE UNIQUE INDEX HKOIT021UID 
    ON HKOTT02_OPREA_ELEM 
    ( KOT02UID )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT022OPR 
    ON HKOTT02_OPREA_ELEM 
    ( KOT02T01OPREAL )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT023ELR 
    ON HKOTT02_OPREA_ELEM 
    ( KOT02ELEMREL )
	
--		  CLOSE NO
--    CLUSTER 
;

CREATE INDEX HKOIT024MRE 
    ON HKOTT02_OPREA_ELEM 
    ( KOT02S01MOTIREL )
	
--		  CLOSE NO
;

CREATE INDEX HKOIT025TEL 
    ON HKOTT02_OPREA_ELEM 
    ( KOT02S00TIPELEM )
	
--		  CLOSE NO
;

