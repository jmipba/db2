CREATE TABLE GECT117_PROVINCIAS                                                 
   (G117PAIS     SMALLINT           NOT NULL                                  
     ,G117PROV     SMALLINT           NOT NULL                                  
     ,G117PROVDC   CHAR(25)           NOT NULL                                  
     ,G117SITU     CHAR(1)            NOT NULL                                  
     );
   CREATE VIEW GECW11701 AS                                                      
      SELECT G117PAIS,G117PROV,G117PROVDC,G117SITU                              
      FROM GECT117_PROVINCIAS;                                                  

   CREATE SYNONYM  GECA11701 FOR GECW11701;                          
