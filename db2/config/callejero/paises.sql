CREATE TABLE GECT116_PAISES                                                     
   (G116PAIS     SMALLINT           NOT NULL                                  
     ,G116PAISD    CHAR(25)           NOT NULL                                  
     ,G116PFISCAL  CHAR(1)            NOT NULL                                  
     ,G116UNEUR    CHAR(1)            NOT NULL                                  
     ,G116RESTO    CHAR(10)           NOT NULL                                  
     ,G116SITU     CHAR(1)            NOT NULL                                  
     ,G116FECM     CHAR(8)            NOT NULL                                  
     ,G116OBSERV   CHAR(60)           NOT NULL                                  
     ,G116CODISO2  CHAR(2)            NOT NULL
     );

CREATE VIEW GECW11601 AS                                                      
      SELECT  G116PAIS,G116PAISD,G116PFISCAL,G116UNEUR                          
     ,G116RESTO,G116SITU,G116FECM,G116OBSERV,G116CODISO2                        
  FROM GECT116_PAISES;

CREATE SYNONYM GECA11601 FOR GECW11601;   