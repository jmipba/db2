CREATE TABLE GECT118_MUNICIPIOS                                                 
   (G118PAIS     SMALLINT           NOT NULL                                  
     ,G118PROV     SMALLINT           NOT NULL                                  
     ,G118MUNI     SMALLINT           NOT NULL                                  
     ,G118MUNI2    SMALLINT           NOT NULL                                  
     ,G118MUNID    CHAR(40)           NOT NULL                                  
     ,G118MUNIDA   CHAR(15)           NOT NULL                                  
     ,G118SITU     CHAR(1)            NOT NULL                                  
     ,G118CODPOS   INTEGER            NOT NULL                                  
                              
     );        
                                                                             
  CREATE VIEW GECW11801 AS   SELECT G118PAIS,G118PROV,G118MUNI,G118MUNI2,G118MUNID,G118MUNIDA               
       ,G118SITU,G118CODPOS     FROM GECT118_MUNICIPIOS;                        
                                                                             
  CREATE SYNONYM  GECA11801 FOR GECW11801;                          
                                                                            

