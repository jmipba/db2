CREATE TABLE IURT014_CALLEJERO
   (B014MUNI         SMALLINT     NOT NULL
  ,B014CVIA         SMALLINT     NOT NULL
  ,B014DVIA         CHAR(30)     NOT NULL
  ,B014CPOS         INTEGER      NOT NULL
  ,B014DESD         CHAR(4)      NOT NULL
  ,B014HAST         CHAR(4)      NOT NULL
  ,B014FALT         INTEGER      NOT NULL
  ,B014FBAJ         INTEGER      NOT NULL
  ,B014TBUS         CHAR(1)      NOT NULL
  ,B014TIME         TIMESTAMP    NOT NULL
  ,B014USER         CHAR(6)      NOT NULL
  ,B014TERM         CHAR(4)      NOT NULL
      );                                           
--                                                                     
    CREATE VIEW IURW01401 AS                                            
SELECT B014MUNI,B014CVIA,B014DVIA,B014CPOS,B014DESD,B014HAST,B014FALT   
 ,B014FBAJ,B014TBUS,B014TIME,B014USER,B014TERM  FROM IURT014_CALLEJERO; 
--                                                                      
--                                                                      
    CREATE SYNONYM IURA01401 FOR IURW01401;                     
--                                                                      
